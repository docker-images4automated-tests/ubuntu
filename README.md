# ubuntu

## Docker images produced by this project from ubuntu (size for 19.10)
* ubuntu (64.2MB)
  * sudo (66.8MB)
* ubuntu:rolling (72.9MB)
  * sudo:rolling (75.7MB)
    * openrc_easy-shutdown:rolling
      * E: Package 'initscripts' has no installation candidate
      * E: Package 'openrc' has no installation candidate
    * openrc_easy:rolling
      * E: Package 'initscripts' has no installation candidate
      * E: Package 'openrc' has no installation candidate

## Notes
* apt-utils installed to avoid a warning message without importance:
  debconf: delaying package configuration, since apt-utils is not installed
  * .4MB compressed (.deb or on docker image) - 1.1MB uncompressed